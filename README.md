<h1 align="center">
Bet Sdk
</h1>

[![pipeline status](https://gitlab.com/hivinau.graffe/bet-sdk/badges/develop/pipeline.svg)](https://gitlab.com/hivinau.graffe/bet-sdk/-/commits/develop)
[![coverage report](https://gitlab.com/hivinau.graffe/bet-sdk/badges/develop/coverage.svg)](https://gitlab.com/hivinau.graffe/bet-sdk/-/commits/develop)

# Contents

[[_TOC_]]

# Features

## List bets

You can find the dedicated documentation here: [/bets](docs/list-bets.md).

## List matches

You can find the dedicated documentation here: [/bets/\{id\}/matches](docs/list-matches.md).

## List pros

You can find the dedicated documentation here: [/matches/\{id\}/pros/](docs/list-pros.md).

# Supported OS & SDK version

- XCode 12.4
- Swift 5.3.2
- Cocoapods 1.10

# How to ?

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Installation

BetSdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BetSdk'
```

There is a [RxSwift](https://github.com/ReactiveX/RxSwift) wrapper in BetSdk.
Just add to your Podfile:

```ruby
pod 'BetSdk/RxSwift'
```

# About

## Author

Hivinau GRAFFE [hivinau.graffe@hotmail.fr](mailto:hivinau.graffe@hotmail.fr)

## License

This project is released under the MIT license. [See LICENSE](LICENSE) for details.
