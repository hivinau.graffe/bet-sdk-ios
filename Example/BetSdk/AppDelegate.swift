import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  // MARK: - UIApplicationDelegate properties
  
  var window: UIWindow?
  
  // MARK: - UIApplicationDelegate methods
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = UINavigationController(rootViewController: HomeViewController())
    window?.makeKeyAndVisible()
    
    return true
  }
}

