import BetSdk
import UIKit

final class HomeViewController: UIViewController {
  
  // MARK: - Constants
  
  private struct Constants {
    static let title = "Home"
    static let backgroundColor = UIColor.white
    static let serverPath = "http://86.208.97.72:8000"
  }
  
  // MARK: - Private properties
  
  private lazy var dataManager = BetDataManager(serverPath: Constants.serverPath)
  
  // MARK: - Init
  
  init() {
    super.init(nibName: "HomeViewController", bundle: .main)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Constants.backgroundColor
    title = Constants.title
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    navigationController?.navigationBar.barTintColor = Constants.backgroundColor
  }
  
  // MARK: - Actions
  
  @IBAction func listBets(_ button: UIButton) {
    let client = BetClient(dataManager: dataManager)
    let controller = BetsViewController(client: client)
    navigationController?.pushViewController(controller, animated: true)
  }
  
  @IBAction func listBetsWithRxSwift(_ button: UIButton) {
    let client = BetClient(dataManager: dataManager)
    let controller = RxBetsViewController(client: client)
    navigationController?.pushViewController(controller, animated: true)
  }
}
