import UIKit

final class BetCell: UITableViewCell {
  
  private struct Images {
    static let lock = UIImage(systemName: "lock")
    static let unlock = UIImage(systemName: "lock.open")
  }
  
  @IBOutlet var leftImageView: UIImageView?
  @IBOutlet var rightImageView: UIImageView?
  @IBOutlet var lockImageView: UIImageView?
  @IBOutlet var priceToUnlockLabel: UILabel?
  
  public var locked: Bool = false {
    didSet {
      lockImageView?.image = locked ? Images.lock : Images.unlock
    }
  }
  
  public var leftImage: UIImage? {
    didSet {
      leftImageView?.image = leftImage
    }
  }
  
  public var rightImage: UIImage? {
    didSet {
      rightImageView?.image = rightImage
    }
  }
  
  public var priceToUnlock: Float = 0 {
    didSet {
      priceToUnlockLabel?.text = "\(priceToUnlock) €"
    }
  }
}
