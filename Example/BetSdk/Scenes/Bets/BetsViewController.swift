import BetSdk
import UIKit

@objc(BetsViewController)
final class BetsViewController: UITableViewController, BetClientDelegate {
  
  // MARK: - Constants
  
  private struct Constants {
    static let title = "Bets"
    static let backgroundColor = UIColor.white
    static let navigationBarColor = UIColor.green
    static let betCellNib = UINib(nibName: "BetCell", bundle: .main)
    static let betCellIdentifier = "BetCell"
  }
  
  // MARK: - Private properties
  
  private let client: BetClient
  private let betsHelper: BetsHelper
  
  // MARK: - Init
  
  init(client: BetClient) {
    self.client = client
    self.betsHelper = BetsHelper()
    super.init(style: .plain)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Constants.backgroundColor
    navigationController?.navigationBar.barTintColor = Constants.navigationBarColor
    title = Constants.title
    
    tableView.register(Constants.betCellNib, forCellReuseIdentifier: Constants.betCellIdentifier)
    tableView.estimatedRowHeight = UITableView.automaticDimension
    tableView.rowHeight = UITableView.automaticDimension
    
    client.delegate = self
    client.bets()
  }
  
  // MARK: - UITableViewDataSource methods
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return betsHelper.bets.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Constants.betCellIdentifier) as? BetCell
    
    let bets = betsHelper.bets
    let bet = bets[indexPath.row]
    if let match = betsHelper.getBetMatches(bet).first {
      let images = betsHelper.getMatchImages(match)
      
      cell?.priceToUnlock = bet.priceToUnlock
      cell?.locked = bet.locked
      cell?.leftImage = images.first?.image
      cell?.rightImage = images.last?.image
    }
    
    return cell ?? super.tableView(tableView, cellForRowAt: indexPath)
  }
  
  // MARK: - BetClientDelegate methods
  
  func clientDidReceive(_ client: BetClient, bets: [BABet]) {
    bets.forEach {
      betsHelper.addBet($0)
      client.betMatches($0.id ?? "")
    }
  }
  
  func clientDidReceive(_ client: BetClient, matches: [BAMatch]) {
    var currentBet: BABet?
    
    for bet in betsHelper.bets {
      for match in matches {
        if match.betId == bet.id {
          currentBet = bet
          break
        }
      }
    }
    
    guard let bet = currentBet else { return }
    
    betsHelper.setMatches(matches, for: bet)
    matches.forEach { match in
      let leftImage = match.teams.first?.image
      let rightImage = match.teams.last?.image
      
      client.image(leftImage ?? "")
      client.image(rightImage ?? "")
    }
  }
  
  func clientDidReceive(_ client: BetClient, image: BAImage) {
    for bet in betsHelper.bets {
      let matches = betsHelper.getBetMatches(bet)
      for match in matches {
        let teams = match.teams
        if teams.first?.image == image.id {
          betsHelper.addImage(image, for: match)
        }
        
        if teams.last?.image == image.id {
          betsHelper.addImage(image, for: match)
        }
      }
    }
    
    tableView.reloadData()
  }
}
