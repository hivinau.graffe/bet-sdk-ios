import BetSdk

final class BetsHelper {
  
  // MARK: - Private properties
  
  private var betsDictionary: [BABet: [BAMatch: [BAImage]]] = [:]
  
  // MARK: - Public properties
  
  var bets: [BABet] { Array(betsDictionary.keys) }
  
  // MARK: - Public methods
  
  func addBet(_ bet: BABet) {
    setMatches([], for: bet)
  }
  
  func setMatches(_ matches: [BAMatch], for bet: BABet) {
    var matchesDictionary: [BAMatch: [BAImage]] = [:]
    matches.forEach { matchesDictionary[$0] = [] }
    
    betsDictionary[bet] = matchesDictionary
  }
  
  func addImage(_ image: BAImage, for match: BAMatch) {
    guard let bet = getMatchBet(match),
          var matchesDictionary = betsDictionary[bet] else { return }
    
    matchesDictionary[match]?.append(image)
    betsDictionary[bet] = matchesDictionary
  }
  
  func getBetMatches(_ bet: BABet) -> [BAMatch] {
    guard let matchesDictionary = betsDictionary[bet] else { return [] }
    return Array(matchesDictionary.keys)
  }
  
  func getMatchImages(_ match: BAMatch) -> [BAImage] {
    guard let bet = getMatchBet(match),
          let matchesDictionary = betsDictionary[bet] else { return [] }
    
    return matchesDictionary.first?.value ?? []
  }
  
  // MARK: - Private methods
  
  private func getMatchBet(_ match: BAMatch) -> BABet? {
    return betsDictionary.keys
      .filter { $0.id == match.betId }
      .first
  }
}
