import BetSdk
import RxSwift
import UIKit

@objc(RxBetsViewController)
final class RxBetsViewController: UITableViewController {
  
  // MARK: - Constants
  
  private struct Constants {
    static let title = "Bets (RxSwift)"
    static let backgroundColor = UIColor.white
    static let navigationBarColor = UIColor.orange
    static let betCellNib = UINib(nibName: "BetCell", bundle: .main)
    static let betCellIdentifier = "BetCell"
  }
  
  // MARK: - Cell data
  
  private struct CellData {
    let bet: BABet
    let matches: [BAMatch]
    let leftImage: UIImage
    let rightImage: UIImage
  }
  
  // MARK: - Private properties
  
  private let client: BetClient
  private let betsHelper: BetsHelper
  private let disposeBag: DisposeBag
  private let betsLoaded: PublishSubject<Void>
  
  // MARK: - Init
  
  init(client: BetClient) {
    self.client = client
    self.betsHelper = BetsHelper()
    self.disposeBag = DisposeBag()
    self.betsLoaded = PublishSubject()
    super.init(style: .plain)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Constants.backgroundColor
    navigationController?.navigationBar.barTintColor = Constants.navigationBarColor
    title = Constants.title
    
    tableView.register(Constants.betCellNib, forCellReuseIdentifier: Constants.betCellIdentifier)
    tableView.estimatedRowHeight = UITableView.automaticDimension
    tableView.rowHeight = UITableView.automaticDimension
    tableView.delegate = nil
    tableView.dataSource = nil
    
    bindBets()
    client.bets()
  }
  
  // MARK: - Private methods
  
  private func bindBets() {
    client.rx.bets
      .subscribe { [weak self] bets in
        bets.forEach {
          self?.betsHelper.addBet($0)
          self?.client.betMatches($0.id ?? "")
        }
      }
      .disposed(by: disposeBag)
    
    client.rx.matches
      .subscribe(onNext: { [weak self] matches in
        guard let bets = self?.betsHelper.bets else { return }
        
        var currentBet: BABet?
        for bet in bets {
          for match in matches {
            if match.betId == bet.id {
              currentBet = bet
              break
            }
          }
        }
        
        guard let bet = currentBet else { return }
        
        self?.betsHelper.setMatches(matches, for: bet)
      })
      .disposed(by: disposeBag)
    
    client.rx.matches
      .map { $0.compactMap { $0.teams.first?.image } }
      .subscribe(onNext: { [weak self] images in
        images.forEach { self?.client.image($0) }
      })
      .disposed(by: disposeBag)
    
    client.rx.matches
      .map { $0.compactMap { $0.teams.last?.image } }
      .subscribe(onNext: { [weak self] images in
        images.forEach { self?.client.image($0) }
      })
      .disposed(by: disposeBag)
    
    client.rx.image.asObservable()
      .map { [weak self] image in
        guard let image = image,
              let bets = self?.betsHelper.bets else { return () }
        
        for bet in bets {
          guard let matches = self?.betsHelper.getBetMatches(bet) else { continue }
          
          for match in matches {
            let teams = match.teams
            if teams.first?.image == image.id {
              self?.betsHelper.addImage(image, for: match)
            }
            
            if teams.last?.image == image.id {
              self?.betsHelper.addImage(image, for: match)
            }
          }
        }
        
        return ()
      }
      .bind(to: betsLoaded)
      .disposed(by: disposeBag)
    
    betsLoaded
      .compactMap { [weak self] in self?.betsHelper.bets }
      .bind(to: tableView.rx.items(cellIdentifier: Constants.betCellIdentifier,
                                   cellType: BetCell.self)) { [weak self] row, bet, cell in
        guard let match = self?.betsHelper.getBetMatches(bet).first,
              let images = self?.betsHelper.getMatchImages(match) else { return }
        
        cell.priceToUnlock = bet.priceToUnlock
        cell.locked = bet.locked
        cell.leftImage = images.first?.image
        cell.rightImage = images.last?.image
      }
      .disposed(by: disposeBag)
  }
}
