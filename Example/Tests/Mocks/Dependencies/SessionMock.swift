@testable import BetSdk

final class SessionMock: NSObject, Session {
  
  // MARK: - Public Mock values
  
  var sendUrlRequestCallsCount = 0
  var sendUrlRequestCalled: Bool { sendUrlRequestCallsCount > 0 }
  var sessionTaskMockValue: SessionTask?
  
  // MARK: - Session methods
  
  func sendUrl(request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> SessionTask {
    sendUrlRequestCallsCount += 1
    return sessionTaskMockValue!
  }
}
