@testable import BetSdk

final class SessionTaskMock: NSObject, SessionTask {
  
  // MARK: - Public Mock values
  
  var currentRequestMockValue: URLRequest?
  
  // MARK: - SessionTask properties
  
  var currentRequest: URLRequest? { currentRequestMockValue }
  
  // MARK: - SessionTask methods
  
  func cancel() {}
  func resume() {}
}
