@testable import BetSdk

final class DecoderMock: Decoder {
  
  // MARK: - Public Mock values
  
  var errorMockValue: Error = ErrorMock.stub
  var decodeCallsCount = 0
  var decodeCalled: Bool { decodeCallsCount > 0 }
  var decodableMockValue: Any?
  
  // MARK: - Decoder methods
  
  func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
    decodeCallsCount += 1
    if decodableMockValue == nil {
      throw errorMockValue
    }
    
    return decodableMockValue as! T
  }
}
