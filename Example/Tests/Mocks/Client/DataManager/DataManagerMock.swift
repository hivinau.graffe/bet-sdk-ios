@testable import BetSdk

final class DataManagerMock: DataManager {
  
  // MARK: - Public Mock values
  
  var urlMockValue: URL?
  var sessionTaskMockValue: SessionTask!
  var decodableMockValue: Decodable!
  var requestFactoryMockValue: RequestFactory!
  var dataMockValue: Data?
  var urlResponseMockValue: URLResponse?
  var errorMockValue: Error?
  
  // MARK: - DataManager methods
  
  func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> SessionTask {
    completionHandler(dataMockValue, urlResponseMockValue, errorMockValue)
    return sessionTaskMockValue
  }
  
  func url(for route: Route) -> URL? {
    return urlMockValue
  }
  
  func decode<T>(_ type: T.Type, from data: Data) -> T? where T : Decodable {
    return decodableMockValue as? T
  }
  
  func requestFactory() -> RequestFactory {
    return requestFactoryMockValue
  }
}
