@testable import BetSdk

final class ProsRequestMock: ProsRequest {
  
  // MARK: - Public properties
  
  var prosMockValue: [BAPro]?
  var errorMockValue: Error?
  
  // MARK: - Init
  
  init() {
    super.init(dataManager: DataManagerMock(), matchId: "")
  }
  
  // MARK: - BetsRequest methods
  
  override func dataTask(completionHandler: @escaping (Result<[BAPro], Error>) -> Void) -> SessionTask? {
    if let prosMockValue = prosMockValue {
      completionHandler(.success(prosMockValue))
    }
    
    if let errorMockValue = errorMockValue {
      completionHandler(.failure(errorMockValue))
    }
    
    return nil
  }
}
