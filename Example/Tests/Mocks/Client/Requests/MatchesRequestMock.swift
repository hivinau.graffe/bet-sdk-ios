@testable import BetSdk

final class MatchesRequestMock: MatchesRequest {
  
  // MARK: - Public properties
  
  var matchesMockValue: [BAMatch]?
  var errorMockValue: Error?
  
  // MARK: - Init
  
  init() {
    super.init(dataManager: DataManagerMock(), betId: "")
  }
  
  // MARK: - BetsRequest methods
  
  override func dataTask(completionHandler: @escaping (Result<[BAMatch], Error>) -> Void) -> SessionTask? {
    if let matchesMockValue = matchesMockValue {
      completionHandler(.success(matchesMockValue))
    }
    
    if let errorMockValue = errorMockValue {
      completionHandler(.failure(errorMockValue))
    }
    
    return nil
  }
}
