@testable import BetSdk

final class RequestFactoryMock: RequestFactory {
  
  // MARK: - Public Mock values
  
  var betsRequestMockValue: BetsRequest!
  var matchesRequestMockValue: MatchesRequest!
  var prosRequestMockValue: ProsRequest!
  var imageRequestMockValue: ImageRequest!
  var goalsRequestMockValue: GoalsRequest!
  
  // MARK: - RequestFactory methods
  
  func betsRequest() -> BetsRequest {
    betsRequestMockValue
  }
  
  func betMatchesRequest(_ betId: String) -> MatchesRequest {
    matchesRequestMockValue
  }
  
  func matchProsRequest(_ matchId: String) -> ProsRequest {
    prosRequestMockValue
  }
  
  func imageRequest(_ filename: String) -> ImageRequest {
    imageRequestMockValue
  }
  
  func matchGoalsRequest(_ matchId: String) -> GoalsRequest {
    goalsRequestMockValue
  }
}
