@testable import BetSdk

final class ImageRequestMock: ImageRequest {
  
  // MARK: - Public properties
  
  var imageMockValue: BAImage?
  var errorMockValue: Error?
  
  // MARK: - Init
  
  init() {
    super.init(dataManager: DataManagerMock(), filename: "")
  }
  
  // MARK: - BetsRequest methods
  
  override func dataTask(completionHandler: @escaping (Result<BAImage, Error>) -> Void) -> SessionTask? {
    if let imageMockValue = imageMockValue {
      completionHandler(.success(imageMockValue))
    }
    
    if let errorMockValue = errorMockValue {
      completionHandler(.failure(errorMockValue))
    }
    
    return nil
  }
}
