@testable import BetSdk

final class BetsRequestMock: BetsRequest {
  
  // MARK: - Public properties
  
  var betsMockValue: [BABet]?
  var errorMockValue: Error?
  
  // MARK: - Init
  
  init() {
    super.init(dataManager: DataManagerMock())
  }
  
  // MARK: - BetsRequest methods
  
  override func dataTask(completionHandler: @escaping (Result<[BABet], Error>) -> Void) -> SessionTask? {
    if let betsMockValue = betsMockValue {
      completionHandler(.success(betsMockValue))
    }
    
    if let errorMockValue = errorMockValue {
      completionHandler(.failure(errorMockValue))
    }
    
    return nil
  }
}
