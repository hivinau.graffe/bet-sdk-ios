@testable import BetSdk

final class GoalsRequestMock: GoalsRequest {
  
  // MARK: - Public properties
  
  var goalsMockValue: [BAGoal]?
  var errorMockValue: Error?
  
  // MARK: - Init
  
  init() {
    super.init(dataManager: DataManagerMock(), matchId: "")
  }
  
  // MARK: - BetsRequest methods
  
  override func dataTask(completionHandler: @escaping (Result<[BAGoal], Error>) -> Void) -> SessionTask? {
    if let goalsMockValue = goalsMockValue {
      completionHandler(.success(goalsMockValue))
    }
    
    if let errorMockValue = errorMockValue {
      completionHandler(.failure(errorMockValue))
    }
    
    return nil
  }
}
