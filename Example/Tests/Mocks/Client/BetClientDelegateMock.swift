import XCTest
@testable import BetSdk

final class BetClientDelegateMock: NSObject, BetClientDelegate {
  
  // MARK: - Public properties
  
  private(set) var betsReceivedByClient: [BABet]?
  private(set) var matchesReceivedByClient: [BAMatch]?
  private(set) var prosReceivedByClient: [BAPro]?
  private(set) var imageReceivedByClient: BAImage?
  private(set) var goalsReceivedByClient: [BAGoal]?
  private(set) var errorReceivedByClient: Error?
  
  public var expectation: XCTestExpectation?
  
  // MARK: - BetClientDelegate methods
  
  func clientDidReceive(_ client: BetClient, bets: [BABet]) {
    betsReceivedByClient = bets
    expectation?.fulfill()
  }
  
  func clientDidReceive(_ client: BetClient, matches: [BAMatch]) {
    matchesReceivedByClient = matches
    expectation?.fulfill()
  }
  
  func clientDidReceive(_ client: BetClient, pros: [BAPro]) {
    prosReceivedByClient = pros
    expectation?.fulfill()
  }
  
  func clientDidReceive(_ client: BetClient, image: BAImage) {
    imageReceivedByClient = image
    expectation?.fulfill()
  }
  
  func clientDidReceive(_ client: BetClient, goals: [BAGoal]) {
    goalsReceivedByClient = goals
    expectation?.fulfill()
  }
  
  func clientDidReceive(_ client: BetClient, error: Error) {
    errorReceivedByClient = error
    expectation?.fulfill()
  }
}
