import XCTest
@testable import BetSdk

final class BetDataManagerIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let expectationTimeout = 2 as TimeInterval
    static let betsUrl = URL(string: "\(serverPath)/bets")
    static let matchesUrl = URL(string: "\(serverPath)/bets/8c5bdcd8-97d8-4ff4-be98-d025413104ee/matches")
    static let invalidMatchesUrl = URL(string: "\(serverPath)/bets/azeaze/matches")
    static let prosUrl = URL(string: "\(serverPath)/matches/bfc7bee5-88e9-4fde-b237-44fda54d5665/pros")
    static let invalidProsUrl = URL(string: "\(serverPath)/matches/azeaze/pros")
    static let imageUrl = URL(string: "\(serverPath)/images/36407e963bdf387f.png")
    static let invalidImageUrl = URL(string: "\(serverPath)/images/azeaze")
    static let goalsUrl = URL(string: "\(serverPath)/matches/adf27f8c-51eb-4fb1-8783-ca9e0e1ee30d/goals")
    static let invalidGoalsUrl = URL(string: "\(serverPath)/matches/azeaze/goals")
    static let responseOkStatus = 200
    static let responseNotFoundStatus = 404
  }
  
  // MARK: - Private properties
  
  private var tested: BetDataManager!
  private let session = URLSession.shared
  private let decoder = JSONDecoder()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = BetDataManager(session: session, decoder: decoder, serverPath: Constants.serverPath)
  }
  
  // MARK: - Tests
  
  func testResponseStatusCode_shouldBeOk_whenGetDataFromBetsUrl() {
    getData(from: Constants.betsUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseOkStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeOk_whenGetDataFromMatchesUrl() {
    getData(from: Constants.matchesUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseOkStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeNotFound_whenGetDataFromInvalidMatchesUrl() {
    getData(from: Constants.invalidMatchesUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseNotFoundStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeOk_whenGetDataFromProsUrl() {
    getData(from: Constants.prosUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseOkStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeNotFound_whenGetDataFromInvalidProsUrl() {
    getData(from: Constants.invalidProsUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseNotFoundStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeOk_whenGetDataFromImageUrl() {
    getData(from: Constants.imageUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseOkStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeNotFound_whenGetDataFromInvalidImageUrl() {
    getData(from: Constants.invalidImageUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseNotFoundStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeOk_whenGetDataFromGoalsUrl() {
    getData(from: Constants.goalsUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseOkStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  func testResponseStatusCode_shouldBeNotFound_whenGetDataFromInvalidGoalsUrl() {
    getData(from: Constants.invalidProsUrl!) { (_: Data?, response: URLResponse?, _: Error?) -> Void in
      XCTAssertEqual(Constants.responseNotFoundStatus, (response as? HTTPURLResponse)?.statusCode)
    }
  }
  
  // MARK: - Private methods
  
  private func getData(from url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
    let urlRequest = URLRequest(url: url)
    let expectation = XCTestExpectation(description: "Get data from url")
    
    let task = tested.dataTask(with: urlRequest) {
      completionHandler($0, $1, $2)
      expectation.fulfill()
    }
    task.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
  }
}
