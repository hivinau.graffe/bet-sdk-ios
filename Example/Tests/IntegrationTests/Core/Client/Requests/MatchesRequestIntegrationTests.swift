import XCTest
@testable import BetSdk

final class MatchesRequestIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let expectationTimeout = 2 as TimeInterval
    static let existingBetId = "23780341-cb80-4f53-a2ee-f2ae7a1c44a1"
    static let invalidBetId = ""
  }
  
  // MARK: - Private properties
  
  private var tested: MatchesRequest!
  private let dataManager = BetDataManager(serverPath: Constants.serverPath)
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = MatchesRequest(dataManager: dataManager, betId: Constants.existingBetId)
  }
  
  func testDataTask_shouldHandleMatches_whenBetIdExists() {
    let expectation = XCTestExpectation(description: "Get result")
    var matches: [BAMatch]?
    
    let task = tested.dataTask { result in
      switch result {
      case .success(let _matches): matches = _matches
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(matches)
  }
  
  func testDataTask_shouldHandleError_whenBetIdIsInvalid() {
    let expectation = XCTestExpectation(description: "Get result")
    var error: Error?
    tested = MatchesRequest(dataManager: dataManager, betId: Constants.invalidBetId)
    
    let task = tested.dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(error)
  }
}
