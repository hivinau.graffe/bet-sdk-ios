import XCTest
@testable import BetSdk

final class GoalsRequestIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let expectationTimeout = 2 as TimeInterval
    static let existingMatchId = "adf27f8c-51eb-4fb1-8783-ca9e0e1ee30d"
    static let invalidMatchId = ""
  }
  
  // MARK: - Private properties
  
  private var tested: GoalsRequest!
  private let dataManager = BetDataManager(serverPath: Constants.serverPath)
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = GoalsRequest(dataManager: dataManager, matchId: Constants.existingMatchId)
  }
  
  func testDataTask_shouldHandleGoals_whenMatchIdExists() {
    let expectation = XCTestExpectation(description: "Get result")
    var goals: [BAGoal]?
    
    let task = tested.dataTask { result in
      switch result {
      case .success(let _goals): goals = _goals
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(goals)
  }
  
  func testDataTask_shouldHandleError_whenMatchIdIsInvalid() {
    let expectation = XCTestExpectation(description: "Get result")
    var error: Error?
    tested = GoalsRequest(dataManager: dataManager, matchId: Constants.invalidMatchId)
    
    let task = tested.dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(error)
  }
}
