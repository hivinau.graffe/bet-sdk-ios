import XCTest
@testable import BetSdk

final class ImageRequestIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let expectationTimeout = 2 as TimeInterval
    static let existingFilename = "36407e963bdf387f.png"
    static let invalidFilename = ""
  }
  
  // MARK: - Private properties
  
  private var tested: ImageRequest!
  private let dataManager = BetDataManager(serverPath: Constants.serverPath)
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = ImageRequest(dataManager: dataManager, filename: Constants.existingFilename)
  }
  
  func testDataTask_shouldHandleImage_whenExistingFilename() {
    let expectation = XCTestExpectation(description: "Get result")
    var image: BAImage?
    
    let task = tested.dataTask { result in
      switch result {
      case .success(let _image): image = _image
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(image)
  }
  
  func testDataTask_shouldHandleError_whenFilenameIsInvalid() {
    let expectation = XCTestExpectation(description: "Get result")
    var error: Error?
    tested = ImageRequest(dataManager: dataManager, filename: Constants.invalidFilename)
    
    let task = tested.dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(error)
  }
}
