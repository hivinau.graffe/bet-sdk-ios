import XCTest
@testable import BetSdk

final class BetsRequestIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let expectationTimeout = 2 as TimeInterval
  }
  
  // MARK: - Private properties
  
  private var tested: BetsRequest!
  private let dataManager = BetDataManager(serverPath: Constants.serverPath)
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = BetsRequest(dataManager: dataManager)
  }
  
  func testDataTask_shouldHandleBets() {
    let expectation = XCTestExpectation(description: "Get result")
    var bets: [BABet]?
    
    let task = tested.dataTask { result in
      switch result {
      case .success(let _bets): bets = _bets
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(bets)
  }
}
