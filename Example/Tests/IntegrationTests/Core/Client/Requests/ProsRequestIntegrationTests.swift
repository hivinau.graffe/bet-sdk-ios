import XCTest
@testable import BetSdk

final class ProsRequestIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let expectationTimeout = 2 as TimeInterval
    static let existingMatchId = "adf27f8c-51eb-4fb1-8783-ca9e0e1ee30d"
    static let invalidMatchId = ""
  }
  
  // MARK: - Private properties
  
  private var tested: ProsRequest!
  private let dataManager = BetDataManager(serverPath: Constants.serverPath)
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = ProsRequest(dataManager: dataManager, matchId: Constants.existingMatchId)
  }
  
  func testDataTask_shouldHandlePros_whenMatchIdExists() {
    let expectation = XCTestExpectation(description: "Get result")
    var pros: [BAPro]?
    
    let task = tested.dataTask { result in
      switch result {
      case .success(let _pros): pros = _pros
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(pros)
  }
  
  func testDataTask_shouldHandleError_whenMatchIdIsInvalid() {
    let expectation = XCTestExpectation(description: "Get result")
    var error: Error?
    tested = ProsRequest(dataManager: dataManager, matchId: Constants.invalidMatchId)
    
    let task = tested.dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
      expectation.fulfill()
    }
    task?.resume()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(error)
  }
}
