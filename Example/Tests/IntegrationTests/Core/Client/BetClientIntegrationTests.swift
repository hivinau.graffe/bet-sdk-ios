import XCTest
@testable import BetSdk

final class BetClientIntegrationTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "https://hivinau.com"
    static let apiConnectionUrl = URL(string: "google.com")
    static let expectationTimeout = 2 as TimeInterval
    static let existingBetId = "23780341-cb80-4f53-a2ee-f2ae7a1c44a1"
    static let invalidBetId = ""
    static let existingMatchId = "f9ca6b42-6088-4f1b-8a74-d5ceb37c4f14"
    static let invalidMatchId = ""
    static let existingFilename = "36407e963bdf387f.png"
    static let invalidFilename = ""
  }
  
  // MARK: - Private properties
  
  private var tested: BetClient!
  private let dataManager = BetDataManager(serverPath: Constants.serverPath)
  private let clientDelegate = BetClientDelegateMock()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = BetClient(dataManager: dataManager)
    tested.delegate = clientDelegate
  }
  
  // MARK: - Tests
  
  func testClientDelegate_shouldReceiveBets_whenBetsCalled() {
    let expectation = XCTestExpectation(description: "Listing bets")
    clientDelegate.expectation = expectation
    
    tested.bets()
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(clientDelegate.betsReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveMatches_whenBetMatchesCalled_withExistingBetId() {
    let expectation = XCTestExpectation(description: "Listing bet matches")
    clientDelegate.expectation = expectation
    
    tested.betMatches(Constants.existingBetId)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(clientDelegate.matchesReceivedByClient)
  }
  
  func testClientDelegateMatchesReceivedByClient_shouldBeNil_whenBetMatchesCalled_withInvalidBetId() {
    let expectation = XCTestExpectation(description: "Failed to list bet matches")
    clientDelegate.expectation = expectation
    
    tested.betMatches(Constants.invalidBetId)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNil(clientDelegate.matchesReceivedByClient)
  }
  
  func testClientDelegate_shouldReceivePros_whenMatchProsCalled_withExistingMatchId() {
    let expectation = XCTestExpectation(description: "Listing match pros")
    clientDelegate.expectation = expectation
    
    tested.matchPros(Constants.existingMatchId)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(clientDelegate.prosReceivedByClient)
  }
  
  func testClientDelegateProsReceivedByClient_shouldBeNil_whenMatchProsCalled_withInvalidMatchId() {
    let expectation = XCTestExpectation(description: "Failed to list match pros")
    clientDelegate.expectation = expectation
    
    tested.matchPros(Constants.invalidMatchId)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNil(clientDelegate.prosReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveImage_whenImageCalled_withExistingFilename() {
    let expectation = XCTestExpectation(description: "Get image")
    clientDelegate.expectation = expectation
    
    tested.image(Constants.existingFilename)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(clientDelegate.imageReceivedByClient)
  }
  
  func testClientDelegateImageReceivedByClient_shouldBeNil_whenImageCalled_withInvalidFilename() {
    let expectation = XCTestExpectation(description: "Failed to get image")
    clientDelegate.expectation = expectation
    
    tested.image(Constants.invalidFilename)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNil(clientDelegate.imageReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveGoals_whenMatchGoalsCalled_withExistingMatchId() {
    let expectation = XCTestExpectation(description: "Listing match goals")
    clientDelegate.expectation = expectation
    
    tested.matchGoals(Constants.existingMatchId)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNotNil(clientDelegate.goalsReceivedByClient)
  }
  
  func testClientDelegateGoalsReceivedByClient_shouldBeNil_whenMatchGoalsCalled_withInvalidMatchId() {
    let expectation = XCTestExpectation(description: "Failed to list match goals")
    clientDelegate.expectation = expectation
    
    tested.matchGoals(Constants.invalidMatchId)
    wait(for: [expectation], timeout: Constants.expectationTimeout)
    
    XCTAssertNil(clientDelegate.goalsReceivedByClient)
  }
}
