import XCTest
@testable import BetSdk

final class MatchesRequestUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let url = URL(string: "google.com")
    static let expectationTimeout = 2 as TimeInterval
    static let httpMethod = "GET"
  }
  
  // MARK: - Private properties
  
  private var tested: MatchesRequest!
  private let dataManager = DataManagerMock()
  private let jsonEncoder = JSONEncoder()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = MatchesRequest(dataManager: dataManager, betId: "id")
  }
  
  // MARK: - Tests
  
  func testDataTask_shouldHandleMatches_whenResultIsSuccessAndDataManagerDataTaskesAndDecodesData() {
    let match = BAMatch(id: "id",
                        betId: "betId",
                        started: false,
                        live: false,
                        date: "date",
                        time: "time",
                        teams: [],
                        prosCount: 0)
    dataManager.dataMockValue = matchesToData([match])
    dataManager.decodableMockValue = [match]
    var matches: [BAMatch]?
    
    dataTask { result in
      switch result {
      case .success(let _matches): matches = _matches
      default: break
      }
    }
    
    XCTAssertNotNil(matches)
  }
  
  func testDataTask_shouldHandleError_whenResultIsFailureAndDataManagerDataTaskesAndThrowsError() {
    dataManager.errorMockValue = ErrorMock.stub
    var error: Error?
    
    dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
    }
    
    XCTAssertNotNil(error)
  }
  
  func testDataTask_shouldReturnATask_withRequestUrlAndHttpMethod() {
    let taskMock = SessionTaskMock()
    var request = URLRequest(url: Constants.url!)
    request.httpMethod = Constants.httpMethod
    taskMock.currentRequestMockValue = request
    dataManager.sessionTaskMockValue = taskMock
    dataManager.urlMockValue = Constants.url
    
    let task = tested.dataTask { _ in }
    
    XCTAssertEqual(Constants.url, task?.currentRequest?.url)
    XCTAssertEqual(Constants.httpMethod, task?.currentRequest?.httpMethod)
  }
  
  // MARK: - Private methods
  
  private func dataTask(completionHandler: @escaping (Result<[BAMatch], Error>) -> Void) {
    dataManager.urlMockValue = Constants.url
    dataManager.sessionTaskMockValue = SessionTaskMock()
    let expectation = XCTestExpectation(description: "Get result")
    
    _ = tested.dataTask {
      completionHandler($0)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: Constants.expectationTimeout)
  }
  
  private func matchesToData(_ matches: [BAMatch]) -> Data? {
    return try? jsonEncoder.encode(matches)
  }
}
