import XCTest
@testable import BetSdk

final class BetsRequestUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let url = URL(string: "google.com")
    static let expectationTimeout = 2 as TimeInterval
    static let httpMethod = "GET"
  }
  
  // MARK: - Private properties
  
  private var tested: BetsRequest!
  private let dataManager = DataManagerMock()
  private let jsonEncoder = JSONEncoder()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = BetsRequest(dataManager: dataManager)
  }
  
  // MARK: - Tests
  
  func testDataTask_shouldHandleBets_whenResultIsSuccessAndDataManagerDataTaskesAndDecodesData() {
    let bet = BABet(id: "id", locked: false, priceToUnlock: 0, matchesCount: 0)
    dataManager.dataMockValue = betsToData([bet])
    dataManager.decodableMockValue = [bet]
    var bets: [BABet]?
    
    dataTask { result in
      switch result {
      case .success(let _bets): bets = _bets
      default: break
      }
    }
    
    XCTAssertNotNil(bets)
  }
  
  func testDataTask_shouldHandleError_whenResultIsFailureAndDataManagerDataTaskesAndThrowsError() {
    dataManager.errorMockValue = ErrorMock.stub
    var error: Error?
    
    dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
    }
    
    XCTAssertNotNil(error)
  }
  
  func testDataTask_shouldReturnATask_withRequestUrlAndHttpMethod() {
    let taskMock = SessionTaskMock()
    var request = URLRequest(url: Constants.url!)
    request.httpMethod = Constants.httpMethod
    taskMock.currentRequestMockValue = request
    dataManager.sessionTaskMockValue = taskMock
    dataManager.urlMockValue = Constants.url
    
    let task = tested.dataTask { _ in }
    
    XCTAssertEqual(Constants.url, task?.currentRequest?.url)
    XCTAssertEqual(Constants.httpMethod, task?.currentRequest?.httpMethod)
  }
  
  // MARK: - Private methods
  
  private func dataTask(completionHandler: @escaping (Result<[BABet], Error>) -> Void) {
    dataManager.urlMockValue = Constants.url
    dataManager.sessionTaskMockValue = SessionTaskMock()
    let expectation = XCTestExpectation(description: "Get result")
    
    _ = tested.dataTask {
      completionHandler($0)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: Constants.expectationTimeout)
  }
  
  private func betsToData(_ bets: [BABet]) -> Data? {
    return try? jsonEncoder.encode(bets)
  }
}
