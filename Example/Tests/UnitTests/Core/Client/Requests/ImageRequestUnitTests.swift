import XCTest
@testable import BetSdk

final class ImageRequestUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let url = URL(string: "google.com")
    static let expectationTimeout = 2 as TimeInterval
    static let httpMethod = "GET"
    static let image = UIImage(named: "image",
                               in: Bundle(for: ImageRequestUnitTests.self),
                               with: nil)
  }
  
  // MARK: - Private properties
  
  private var tested: ImageRequest!
  private let dataManager = DataManagerMock()
  private let jsonEncoder = JSONEncoder()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = ImageRequest(dataManager: dataManager, filename: "id")
  }
  
  // MARK: - Tests
  
  func testDataTask_shouldHandleImage_whenResultIsSuccessAndDataManagerDataTaskes() {
    dataManager.dataMockValue = Constants.image?.pngData()
    var image: BAImage?
    
    dataTask { result in
      switch result {
      case .success(let _image): image = _image
      default: break
      }
    }
    
    XCTAssertNotNil(image)
  }
  
  func testDataTask_shouldHandleError_whenResultIsFailureAndDataManagerDataTaskesAndThrowsError() {
    dataManager.errorMockValue = ErrorMock.stub
    var error: Error?
    
    dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
    }
    
    XCTAssertNotNil(error)
  }
  
  func testDataTask_shouldReturnATask_withRequestUrlAndHttpMethod() {
    let taskMock = SessionTaskMock()
    var request = URLRequest(url: Constants.url!)
    request.httpMethod = Constants.httpMethod
    taskMock.currentRequestMockValue = request
    dataManager.sessionTaskMockValue = taskMock
    dataManager.urlMockValue = Constants.url
    
    let task = tested.dataTask { _ in }
    
    XCTAssertEqual(Constants.url, task?.currentRequest?.url)
    XCTAssertEqual(Constants.httpMethod, task?.currentRequest?.httpMethod)
  }
  
  // MARK: - Private methods
  
  private func dataTask(completionHandler: @escaping (Result<BAImage, Error>) -> Void) {
    dataManager.urlMockValue = Constants.url
    dataManager.sessionTaskMockValue = SessionTaskMock()
    let expectation = XCTestExpectation(description: "Get result")
    
    _ = tested.dataTask {
      completionHandler($0)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: Constants.expectationTimeout)
  }
}
