import XCTest
@testable import BetSdk

final class GoalsRequestUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let url = URL(string: "google.com")
    static let expectationTimeout = 2 as TimeInterval
    static let httpMethod = "GET"
  }
  
  // MARK: - Private properties
  
  private var tested: GoalsRequest!
  private let dataManager = DataManagerMock()
  private let jsonEncoder = JSONEncoder()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = GoalsRequest(dataManager: dataManager, matchId: "id")
  }
  
  // MARK: - Tests
  
  func testDataTask_shouldHandleGoals_whenResultIsSuccessAndDataManagerDataTaskesAndDecodesData() {
    let goal = BAGoal(id: "id", matchId: "matchId", teamId: "teamId", scorer: "scorer", time: 0)
    dataManager.dataMockValue = goalsToData([goal])
    dataManager.decodableMockValue = [goal]
    var goals: [BAGoal]?
    
    dataTask { result in
      switch result {
      case .success(let _goals): goals = _goals
      default: break
      }
    }
    
    XCTAssertNotNil(goals)
  }
  
  func testDataTask_shouldHandleError_whenResultIsFailureAndDataManagerDataTaskesAndThrowsError() {
    dataManager.errorMockValue = ErrorMock.stub
    var error: Error?
    
    dataTask { result in
      switch result {
      case .failure(let _error): error = _error
      default: break
      }
    }
    
    XCTAssertNotNil(error)
  }
  
  func testDataTask_shouldReturnATask_withRequestUrlAndHttpMethod() {
    let taskMock = SessionTaskMock()
    var request = URLRequest(url: Constants.url!)
    request.httpMethod = Constants.httpMethod
    taskMock.currentRequestMockValue = request
    dataManager.sessionTaskMockValue = taskMock
    dataManager.urlMockValue = Constants.url
    
    let task = tested.dataTask { _ in }
    
    XCTAssertEqual(Constants.url, task?.currentRequest?.url)
    XCTAssertEqual(Constants.httpMethod, task?.currentRequest?.httpMethod)
  }
  
  // MARK: - Private methods
  
  private func dataTask(completionHandler: @escaping (Result<[BAGoal], Error>) -> Void) {
    dataManager.urlMockValue = Constants.url
    dataManager.sessionTaskMockValue = SessionTaskMock()
    let expectation = XCTestExpectation(description: "Get result")
    
    _ = tested.dataTask {
      completionHandler($0)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: Constants.expectationTimeout)
  }
  
  private func goalsToData(_ goals: [BAGoal]) -> Data? {
    return try? jsonEncoder.encode(goals)
  }
}
