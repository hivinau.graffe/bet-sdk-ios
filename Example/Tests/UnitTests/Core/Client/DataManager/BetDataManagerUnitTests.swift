import XCTest
@testable import BetSdk

final class BetDataManagerUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let serverPath = "http://86.208.97.72:8000"
    static let someUrl = URL(string: "google.com")
    static let betsUrl = URL(string: "\(serverPath)/bets")
    static let matchesUrl = URL(string: "\(serverPath)/bets/betId/matches")
    static let prosUrl = URL(string: "\(serverPath)/matches/matchId/pros")
    static let imageUrl = URL(string: "\(serverPath)/images/image")
    static let goalsUrl = URL(string: "\(serverPath)/matches/matchId/goals")
  }
  
  // MARK: - Private properties
  
  private var tested: BetDataManager!
  private let session = SessionMock()
  private let decoder = DecoderMock()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = BetDataManager(session: session, decoder: decoder, serverPath: Constants.serverPath)
  }
  
  // MARK: - Tests
  
  func testDataTask_shouldReturnSessionDataTaskResult() {
    session.sessionTaskMockValue = SessionTaskMock()
    let urlRequest = URLRequest(url: Constants.someUrl!)
    let completionHandler = { (_: Data?, _: URLResponse?, _: Error?) -> Void in }
    
    XCTAssertTrue(tested.dataTask(with: urlRequest, completionHandler: completionHandler) is SessionTaskMock)
    XCTAssertTrue(session.sendUrlRequestCalled)
  }
  
  func testUrlForBets_shouldReturnBetsUrl() {
    XCTAssertEqual(Constants.betsUrl, tested.url(for: .bets))
  }
  
  func testUrlForMatches_shouldReturnMatchesUrl() {
    XCTAssertEqual(Constants.matchesUrl, tested.url(for: .matches("betId")))
  }
  
  func testUrlForPros_shouldReturnProsUrl() {
    XCTAssertEqual(Constants.prosUrl, tested.url(for: .pros("matchId")))
  }
  
  func testUrlForImage_shouldReturnImageUrl() {
    XCTAssertEqual(Constants.imageUrl, tested.url(for: .image("image")))
  }
  
  func testUrlForGoals_shouldReturnGoalsUrl() {
    XCTAssertEqual(Constants.goalsUrl, tested.url(for: .goals("matchId")))
  }
  
  func testDecoderDecode_shouldBeCalled_whenDecode() {
    let data = Data()
    let mock = DecodableMock()
    decoder.decodableMockValue = mock

    let decoded = tested.decode(type(of: mock), from: data)

    XCTAssertTrue(decoder.decodeCalled)
    XCTAssertNotNil(decoded)
  }
  
  func testDecode_shouldReturnNil_whenDecoderDecodesAndThrowsError() {
    let mock = DecodableMock()
    let data = Data()
    decoder.decodableMockValue = nil
    decoder.errorMockValue = ErrorMock.stub
    
    XCTAssertNil(tested.decode(type(of: mock), from: data))
  }
  
  func testRequestFactory_shouldReturnABetRequestFactory() {
    XCTAssertTrue(tested.requestFactory() is BetRequestFactory)
  }
}
