import XCTest
@testable import BetSdk

final class BAProUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let proJsonString =
      """
        {
          "id": "id",
          "match_id": "matchId",
          "label": "label",
          "type": "type",
          "is_won": true
        }
      """
      .replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Private properties
  
  private let jsonEncoder = JSONEncoder()
  private let jsonDecoder = JSONDecoder()
  
  // MARK: - Tests
  
  func testJsonDecoder_shouldDecodeJsonStringToValidPro() {
    let data = Constants.proJsonString.data(using: .utf8)!
    let pro = try! jsonDecoder.decode(BAPro.self, from: data)
    
    XCTAssertEqual("id", pro.id)
    XCTAssertEqual("matchId", pro.matchId)
    XCTAssertEqual("label", pro.label)
    XCTAssertEqual("type", pro.type)
    XCTAssertEqual(true, pro.won)
  }
  
  func testJsonEncoder_shouldEncodeProToJsonString() {
    let pro = BAPro(id: "id", matchId: "matchId", label: "label", won: true, type: "type")
    let data = try! jsonEncoder.encode(pro)
    let jsonString = String(decoding: data, as: UTF8.self)
    
    XCTAssertEqual(Constants.proJsonString, jsonString)
  }
  
  func testProDescription_shoudBeAJsonString() {
    let pro = BAPro(id: "id", matchId: "matchId", label: "label", won: true, type: "type")
    
    XCTAssertEqual(Constants.proJsonString, "\(pro)")
  }
}
