import XCTest
@testable import BetSdk

final class BAGoalUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let goalJsonString =
      """
        {
          "time": 0,
          "id": "id",
          "match_id": "matchId",
          "team_id": "teamId",
          "scorer": "scorer"
        }
      """
      .replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Private properties
  
  private let jsonEncoder = JSONEncoder()
  private let jsonDecoder = JSONDecoder()
  
  // MARK: - Tests
  
  func testJsonDecoder_shouldDecodeJsonStringToValidGoal() {
    let data = Constants.goalJsonString.data(using: .utf8)!
    let goal = try! jsonDecoder.decode(BAGoal.self, from: data)
    
    XCTAssertEqual("id", goal.id)
    XCTAssertEqual("scorer", goal.scorer)
    XCTAssertEqual("matchId", goal.matchId)
    XCTAssertEqual("teamId", goal.teamId)
    XCTAssertEqual(0, goal.time)
  }
  
  func testJsonEncoder_shouldEncodeGoalToJsonString() {
    let goal = BAGoal(id: "id", matchId: "matchId", teamId: "teamId", scorer: "scorer", time: 0)
    let data = try! jsonEncoder.encode(goal)
    let jsonString = String(decoding: data, as: UTF8.self)
    
    XCTAssertEqual(Constants.goalJsonString, jsonString)
  }
  
  func testGoalDescription_shoudBeAJsonString() {
    let goal = BAGoal(id: "id", matchId: "matchId", teamId: "teamId", scorer: "scorer", time: 0)
    
    XCTAssertEqual(Constants.goalJsonString, "\(goal)")
  }
}
