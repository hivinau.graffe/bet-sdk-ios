import XCTest
@testable import BetSdk

final class BATeamUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let teamJsonString =
      """
        {
          "id": "id",
          "colors": {
            "secondary": "secondary",
            "primary": "primary",
            "detail": "detail",
            "background": "background"
          },
          "name": "name",
          "image": "image"
        }
      """
      .replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Private properties
  
  private let jsonEncoder = JSONEncoder()
  private let jsonDecoder = JSONDecoder()
  
  // MARK: - Tests
  
  func testJsonDecoder_shouldDecodeJsonStringToValidTeam() {
    let data = Constants.teamJsonString.data(using: .utf8)!
    let team = try! jsonDecoder.decode(BATeam.self, from: data)
    
    XCTAssertEqual("id", team.id)
    XCTAssertEqual("name", team.name)
    XCTAssertEqual("image", team.image)
  }
  
  func testJsonEncoder_shouldEncodeProToJsonString() {
    let colors = BAColors(background: "background", primary: "primary", secondary: "secondary", detail: "detail")
    let team = BATeam(id: "id", name: "name", image: "image", colors: colors)
    let data = try! jsonEncoder.encode(team)
    let jsonString = String(decoding: data, as: UTF8.self)
    
    XCTAssertEqual(Constants.teamJsonString, jsonString)
  }
  
  func testTeamDescription_shoudBeAJsonString() {
    let colors = BAColors(background: "background", primary: "primary", secondary: "secondary", detail: "detail")
    let team = BATeam(id: "id", name: "name", image: "image", colors: colors)
    
    XCTAssertEqual(Constants.teamJsonString, "\(team)")
  }
}
