import XCTest
@testable import BetSdk

final class BABetUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let betJsonString =
      """
        {
          "id": "id",
          "price_to_unlock": 0.75,
          "is_locked": true,
          "matches_count": 0
        }
      """
      .replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Private properties
  
  private let jsonEncoder = JSONEncoder()
  private let jsonDecoder = JSONDecoder()
  
  // MARK: - Tests
  
  func testJsonDecoder_shouldDecodeJsonStringToValidBet() {
    let data = Constants.betJsonString.data(using: .utf8)!
    let bet = try! jsonDecoder.decode(BABet.self, from: data)
    
    XCTAssertEqual("id", bet.id)
    XCTAssertEqual(true, bet.locked)
    XCTAssertEqual(0.75, bet.priceToUnlock)
  }
  
  func testJsonEncoder_shouldEncodeBetToJsonString() {
    let bet = BABet(id: "id",
                    locked: true,
                    priceToUnlock: 0.75,
                    matchesCount: 0)
    let data = try! jsonEncoder.encode(bet)
    let jsonString = String(decoding: data, as: UTF8.self)
    
    XCTAssertEqual(Constants.betJsonString, jsonString)
  }
  
  func testBetDescription_shoudBeAJsonString() {
    let bet = BABet(id: "id",
                    locked: true,
                    priceToUnlock: 0.75,
                    matchesCount: 0)
    
    XCTAssertEqual(Constants.betJsonString, "\(bet)")
  }
}

