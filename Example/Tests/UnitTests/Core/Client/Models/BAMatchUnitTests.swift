import XCTest
@testable import BetSdk

final class BAMatchUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let matchJsonString =
      """
        {
          "bet_id": "betId",
          "is_started": true,
          "time": "time",
          "id": "id",
          "teams": [],
          "is_live": true,
          "date": "date",
          "pros_count": 0
        }
      """
      .replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Private properties
  
  private let jsonEncoder = JSONEncoder()
  private let jsonDecoder = JSONDecoder()
  
  // MARK: - Tests
  
  func testJsonDecoder_shouldDecodeJsonStringToValidMatch() {
    let data = Constants.matchJsonString.data(using: .utf8)!
    let match = try! jsonDecoder.decode(BAMatch.self, from: data)
    
    XCTAssertEqual("id", match.id)
    XCTAssertEqual(true, match.started)
    XCTAssertEqual(true, match.live)
    XCTAssertEqual("date", match.date)
    XCTAssertEqual("time", match.time)
    XCTAssertTrue(match.teams.isEmpty)
  }
  
  func testJsonEncoder_shouldEncodeMatchToJsonString() {
    let match = BAMatch(id: "id",
                        betId: "betId",
                        started: true,
                        live: true,
                        date: "date",
                        time: "time",
                        teams: [],
                        prosCount: 0)
    let data = try! jsonEncoder.encode(match)
    let jsonString = String(decoding: data, as: UTF8.self)
    
    XCTAssertEqual(Constants.matchJsonString, jsonString)
  }
  
  func testMatchDescription_shoudBeAJsonString() {
    let match = BAMatch(id: "id",
                        betId: "betId",
                        started: true,
                        live: true,
                        date: "date",
                        time: "time",
                        teams: [],
                        prosCount: 0)
    
    XCTAssertEqual(Constants.matchJsonString, "\(match)")
  }
}
