import XCTest
@testable import BetSdk

final class BetClientUnitTests: XCTestCase {
  
  // MARK: - Constants
  
  private struct Constants {
    static let apiConnectionUrl = URL(string: "google.com")
  }
  
  // MARK: - Private properties
  
  private var tested: BetClient!
  private let betsRequest = BetsRequestMock()
  private let matchesRequest = MatchesRequestMock()
  private let prosRequest = ProsRequestMock()
  private let imageRequest = ImageRequestMock()
  private let goalsRequest = GoalsRequestMock()
  private let clientDelegate = BetClientDelegateMock()
  private lazy var requestFactory: RequestFactoryMock = {
    let requestFactory = RequestFactoryMock()
    
    requestFactory.betsRequestMockValue = betsRequest
    requestFactory.matchesRequestMockValue = matchesRequest
    requestFactory.prosRequestMockValue = prosRequest
    requestFactory.imageRequestMockValue = imageRequest
    requestFactory.goalsRequestMockValue = goalsRequest
    
    return requestFactory
  }()
  private lazy var dataManager: DataManagerMock = { [requestFactory] in
    let dataManager = DataManagerMock()
    
    dataManager.requestFactoryMockValue = requestFactory
    dataManager.urlMockValue = Constants.apiConnectionUrl
    
    return dataManager
  }()
  
  // MARK: - Lifecycle
  
  override func setUp() {
    super.setUp()
    
    tested = BetClient(dataManager: dataManager)
    tested.delegate = clientDelegate
  }
  
  // MARK: - Tests
  
  func testClientDelegate_shouldReceiveBets_whenBetsCalledAndBetsRequestFetchesBets() {
    betsRequest.betsMockValue = []

    tested.bets()

    XCTAssertNotNil(clientDelegate.betsReceivedByClient)
    XCTAssertNil(clientDelegate.errorReceivedByClient)
  }

  func testClientDelegate_shouldReceiveMatches_whenBetMatchesCalledAndMatchesRequestFetchesMatches() {
    matchesRequest.matchesMockValue = []

    tested.betMatches("")

    XCTAssertNotNil(clientDelegate.matchesReceivedByClient)
    XCTAssertNil(clientDelegate.errorReceivedByClient)
  }

  func testClientDelegate_shouldReceivePros_whenMatchProsCalledAndProsRequestFetchesPros() {
    prosRequest.prosMockValue = []

    tested.matchPros("")

    XCTAssertNotNil(clientDelegate.prosReceivedByClient)
    XCTAssertNil(clientDelegate.errorReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveImage_whenImageCalledAndImageRequestFetchesImage() {
    imageRequest.imageMockValue = BAImage(id: nil, image: nil)

    tested.image("")

    XCTAssertNotNil(clientDelegate.imageReceivedByClient)
    XCTAssertNil(clientDelegate.errorReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveGoals_whenMatchGoalsCalledAndGoalsRequestFetchesGoals() {
    goalsRequest.goalsMockValue = []

    tested.matchGoals("")

    XCTAssertNotNil(clientDelegate.goalsReceivedByClient)
    XCTAssertNil(clientDelegate.errorReceivedByClient)
  }

  func testClientDelegate_shouldReceiveError_whenBetsCalledAndBetsRequestThrowsError() {
    betsRequest.errorMockValue = ErrorMock.stub

    tested.bets()

    XCTAssertNotNil(clientDelegate.errorReceivedByClient)
    XCTAssertNil(clientDelegate.betsReceivedByClient)
  }

  func testClientDelegate_shouldReceiveError_whenBetMatchesCalledAndMatchesRequestThrowsError() {
    matchesRequest.errorMockValue = ErrorMock.stub

    tested.betMatches("")

    XCTAssertNotNil(clientDelegate.errorReceivedByClient)
    XCTAssertNil(clientDelegate.matchesReceivedByClient)
  }

  func testClientDelegate_shouldReceiveError_whenMatchProsCalledAndProsRequestThrowsError() {
    prosRequest.errorMockValue = ErrorMock.stub

    tested.matchPros("")

    XCTAssertNotNil(clientDelegate.errorReceivedByClient)
    XCTAssertNil(clientDelegate.prosReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveError_whenImageCalledAndImageRequestThrowsError() {
    imageRequest.errorMockValue = ErrorMock.stub

    tested.image("")

    XCTAssertNotNil(clientDelegate.errorReceivedByClient)
    XCTAssertNil(clientDelegate.imageReceivedByClient)
  }
  
  func testClientDelegate_shouldReceiveError_whenMatchGoalsCalledAndGoalsRequestThrowsError() {
    goalsRequest.errorMockValue = ErrorMock.stub

    tested.matchGoals("")

    XCTAssertNotNil(clientDelegate.errorReceivedByClient)
    XCTAssertNil(clientDelegate.goalsReceivedByClient)
  }
}
