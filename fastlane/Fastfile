update_fastlane

default_platform(:ios)

platform :ios do

  lane :test do
    cocoapods(clean_install: true, podfile: 'Example/Podfile')
    scan
  end
  
  lane :deploy do |arguments|
    version = version_bump_podspec(path: "BetSdk.podspec", bump_type: arguments[:bump])
    message = "[Prepare] - Release #{version} is ready to deploy 🚀"
    last_commits = format_last_commits()
    content = "## [#{version}]\n\n#{last_commits}"
    current_branch = sh("git rev-parse --abbrev-ref HEAD")
    
    if git_branch != "develop"
      sh("git checkout develop")
    end
    
    increment_version_number(
      bump_type: arguments[:bump],
      version_number: version,
      xcodeproj: './Example/BetSdk.xcodeproj'
    )
    
    append_content_changelog(content)
    
    sh("git add -u")
    sh("git commit --amend -m \"#{message}\"")
    sh("git push --force origin \"develop\"")

    sh("git checkout \"master\"")
    sh("git rebase \"develop\"")
    sh("git merge \"develop\" --no-commit --no-ff")
    sh("git push --force origin \"master\"")

    sh("git tag #{version}")
    sh("git push --tags")
  end
  
  def format_last_commits
    commits = sh("git log $(git describe --tags $(git rev-list --tags --max-count=1))..HEAD --pretty=\"format:%s #%h\"").split("\n")
    verbs = ["Added", "Changed", "Removed", "Fixed"]
    
    changes = ""
    verbs.each { |verb|
      entries = ""
      commits.each { |commit|
        if commit.include? "#{verb}"
          entries += "#{commit}".sub("[#{verb}]", "").strip
          entries += "\n"
        end
      }
      if entries.empty? == false
        changes += "### #{verb}\n" + entries + "\n"
      end
    }
    
    return changes
  end
  
  def append_content_changelog(content)
    filename = "CHANGELOG.md"
    changelog = "## [WIP]\n\n#{content}"
    Dir.chdir("..") do
      text = File.read(filename)
      new_contents = text.gsub("## [WIP]\n\n", changelog)
      File.open(filename, "w") {|file| file.puts new_contents }
    end
  end

end
