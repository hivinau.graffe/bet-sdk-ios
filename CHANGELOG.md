# Change Log
All notable changes to this project will be documented in this file.

## [WIP]

## [1.4.4]

## [1.4.3]

### Changed
- Update `serverPath` #38f1c57
- Update `RxSwift` to `6.2.0` #76f6422

## [1.4.2]

### Changed
- Update `serverPath` #6e02a19

## [1.4.1]

### Changed
- Update RxSwift and RxCocoa to `6.1.0` #9fa0030

## [1.4.0]

### Added
- Add `colors` option #7af7cd7

## [1.3.2]

### Changed
- Replace `public` modifier by `open` #8ca1e8a

## [1.3.1]

### Fixed
- Add `public` modifier #3469be9

## [1.3.0]

### Added
- Setup request for match goals #70f886c
- Add `Pro.type` option #9e92ea0
- Add `BetDataManager.serverPath` option #0e6a4c0

## [1.2.0]

### Added
- Add `parent identifier` #df8e071

## [1.1.0]

### Added
- Add `pros_count` property #2b62e2b
- Add `matches_count` property #1db27f7

### Changed
- Replace `CD` by `BA` #30804f6

## [1.0.3]

### Added
- Setup request for image #825980d

## [1.0.2]

### Added
- Add a lane `deploy` to merge `develop` to `master` #b4cabf9

### Fixed
- Add changes before comitting #11b700a
- Replace `$` by `#` #9612bea


## [1.0.1]

### Added
- Add RxSwift support #05942f5
- Add RxSwift support #3fe8e34
- Add option to set local or remote api address #85cd95b
- Add documentation #8fd21d7
- Setup CI #8ad787d
- Setup cocoapod specs #df7fde1
- Setup example app #99255ad

### Changed
- Replace `currentTask` by `tasks` manager #e05ef85
