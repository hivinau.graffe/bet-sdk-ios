#
# Be sure to run `pod lib lint BetSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BetSdk'
  s.version          = '1.4.4'
  s.summary          = 'iOS SDK to interact with Bet Api.'

  s.description      = <<-DESC
This library helps you to perform RESTful requests to Bet Api.
                       DESC

  s.homepage         = 'https://gitlab.com/hivinau.graffe/bet-sdk-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'hivinau' => 'hivinau.graffe@hotmail.fr' }
  s.source           = { :git => 'https://gitlab.com/hivinau.graffe/bet-sdk-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'
  s.default_subspec = 'Core'
  s.static_framework = true
  s.swift_version = '5.0'
  
  s.subspec 'Core' do |c|
    c.source_files = 'BetSdk/Classes/Core/**/*'
  end
  
  s.subspec 'RxSwift' do |rx|
    rx.dependency 'BetSdk/Core'
    rx.dependency 'RxSwift', '~> 6.2.0'
    rx.dependency 'RxCocoa', '~> 6.2.0'
    rx.source_files = 'BetSdk/Classes/RxSwift/**/*'
  end
end
