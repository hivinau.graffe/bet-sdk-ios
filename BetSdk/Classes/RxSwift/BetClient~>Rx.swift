import RxCocoa
import RxSwift

extension Reactive where Base: BetClient {
  
  // MARK: - Public properties
  
  public var delegate: DelegateProxy<BetClient, BetClientDelegate> { BetClientDelegateProxy.proxy(for: base) }
  
  public var bets: ControlEvent<[BABet]> {
    let source = delegate
      .methodInvoked(#selector(BetClientDelegate.clientDidReceive(_:bets:)))
      .map { try castOrThrow([BABet].self, $0[1] as? [BABet] ?? []) }
    
    return ControlEvent(events: source)
  }
  
  public var matches: ControlEvent<[BAMatch]> {
    let source = delegate
      .methodInvoked(#selector(BetClientDelegate.clientDidReceive(_:matches:)))
      .map { try castOrThrow([BAMatch].self, $0[1] as? [BAMatch] ?? []) }
    
    return ControlEvent(events: source)
  }
  
  public var pros: ControlEvent<[BAPro]> {
    let source = delegate
      .methodInvoked(#selector(BetClientDelegate.clientDidReceive(_:pros:)))
      .map { try castOrThrow([BAPro].self, $0[1] as? [BAPro] ?? []) }
    
    return ControlEvent(events: source)
  }
  
  public var error: ControlEvent<Error> {
    let source = delegate
      .methodInvoked(#selector(BetClientDelegate.clientDidReceive(_:error:)))
      .map { try castOrThrow(Error.self, $0[1] as? Error as Any) }
    
    return ControlEvent(events: source)
  }
  
  public var image: ControlEvent<BAImage?> {
    let source = delegate
      .methodInvoked(#selector(BetClientDelegate.clientDidReceive(_:image:)))
      .map { try castOrThrow(BAImage?.self, $0[1] as? BAImage as Any) }
    
    return ControlEvent(events: source)
  }
  
  public var goals: ControlEvent<[BAGoal]> {
    let source = delegate
      .methodInvoked(#selector(BetClientDelegate.clientDidReceive(_:goals:)))
      .map { try castOrThrow([BAGoal].self, $0[1] as? [BAGoal] ?? []) }
    
    return ControlEvent(events: source)
  }
  
  // MARK: - Public methods
  
  public func setDelegate(_ delegate: BetClientDelegate) -> Disposable {
    return BetClientDelegateProxy.installForwardDelegate(delegate,
                                                         retainDelegate: false,
                                                         onProxyForObject: base)
  }
}
