import RxCocoa
import RxSwift

extension BetClient: HasDelegate {
    public typealias Delegate = BetClientDelegate
}
