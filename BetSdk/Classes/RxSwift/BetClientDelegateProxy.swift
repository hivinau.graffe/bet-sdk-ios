import RxCocoa
import RxSwift

public final class BetClientDelegateProxy: DelegateProxy<BetClient, BetClientDelegate>, DelegateProxyType, BetClientDelegate {

  // MARK: - Private properties
  
  public private(set) var parentObject: ParentObject?
  
  // MARK: - Init
  
  public init(parentObject: ParentObject) {
    self.parentObject = parentObject
    super.init(parentObject: parentObject, delegateProxy: BetClientDelegateProxy.self)
  }
  
  // MARK: - DelegateProxyType methods
  
  public static func registerKnownImplementations() {
    self.register { BetClientDelegateProxy(parentObject: $0) }
  }
  
  public static func currentDelegate(for object: BetClient) -> BetClientDelegate? {
    return object.delegate
  }
  
  public static func setCurrentDelegate(_ delegate: BetClientDelegate?, to object: BetClient) {
    object.delegate = delegate
  }
}
