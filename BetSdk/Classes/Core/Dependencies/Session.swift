public protocol Session: NSObjectProtocol {
  func sendUrl(request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> SessionTask
}

extension URLSession: Session {
  
  public func sendUrl(request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> SessionTask {
    return self.dataTask(with: request, completionHandler: completionHandler) as SessionTask
  }
}
