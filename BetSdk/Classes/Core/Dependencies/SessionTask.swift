public protocol SessionTask: NSObjectProtocol {
  var currentRequest: URLRequest? { get }
  
  func cancel()
  func resume()
}

extension URLSessionTask: SessionTask {}
