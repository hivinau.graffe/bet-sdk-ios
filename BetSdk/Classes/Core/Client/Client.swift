public protocol Client: NSObjectProtocol {
  func bets()
  func betMatches(_ betId: String)
  func matchPros(_ matchId: String)
  func image(_ filename: String)
  func matchGoals(_ matchId: String)
}
