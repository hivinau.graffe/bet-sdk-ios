import Foundation

public final class BetDataManager: DataManager {
  
  // MARK: - Private properties
  
  private let serverPath: String
  private let session: Session
  private let decoder: Decoder
  
  // MARK: - Init
  
  public init(session: Session = URLSession.shared,
              decoder: Decoder = JSONDecoder(),
              serverPath: String) {
    self.serverPath = serverPath
    self.session = session
    self.decoder = decoder
  }
  
  // MARK: - DataManager methods
  
  public func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> SessionTask {
    return session.sendUrl(request: request, completionHandler: completionHandler)
  }
  
  public func url(for route: Route) -> URL? {
    return URL(string: "\(serverPath)/\(route)")
  }
  
  public func decode<T>(_ type: T.Type, from data: Data) -> T? where T : Decodable {
    do {
      return try decoder.decode(type, from: data)
    } catch {
      return nil
    }
  }
  
  public func requestFactory() -> RequestFactory {
    return BetRequestFactory(dataManager: self)
  }
}
