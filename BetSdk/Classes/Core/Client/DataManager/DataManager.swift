public protocol DataManager {
  func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> SessionTask
  func url(for route: Route) -> URL?
  func decode<T>(_ type: T.Type, from data: Data) -> T? where T : Decodable
  func requestFactory() -> RequestFactory
}
