public enum Route: CustomStringConvertible {
  case bets
  case matches(String)
  case pros(String)
  case image(String)
  case goals(String)
  
  // MARK: - CustomStringConvertible properties
  
  public var description: String {
    switch self {
    case .bets: return "bets"
    case .matches(let betId): return "bets/\(betId)/matches"
    case .pros(let matchId): return "matches/\(matchId)/pros"
    case .image(let filename): return "images/\(filename)"
    case .goals(let matchId): return "matches/\(matchId)/goals"
    }
  }
}
