public class Headers {
  
  // MARK: - Private properties
  
  private var dictionary: [String: String]
  
  // MARK: - Public properties
  
  public var keys: [String] { dictionary.keys.sorted() }
  
  // MARK: - Init
  
  init() {
    dictionary = [:]
  }
  
  // MARK: - Public methods
  
  public func addValue(_ value: String, for key: String) {
    dictionary[key] = value
  }
  
  public func value(for key: String) -> String? {
    return dictionary[key] ?? nil
  }
}
