public enum Method: String, CustomStringConvertible {
  case get = "GET"
  case post = "POST"
  
  // MARK: - CustomStringConvertible properties
  
  public var description: String { self.rawValue }
}
