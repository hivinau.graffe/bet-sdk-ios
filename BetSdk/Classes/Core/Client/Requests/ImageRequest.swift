public class ImageRequest: BaseRequest<BAImage> {
  
  private let filename: String
  
  // MARK: - Init
  
  public init(dataManager: DataManager, filename: String) {
    self.filename = filename
    super.init(dataManager: dataManager,
               method: .get,
               route: .image(filename),
               headers: nil,
               dispatchQueue: .main)
  }
  
  // MARK: - BaseRequest methods
  
  public override func dataTask(completionHandler: @escaping (Result<BAImage, Error>) -> Void) -> SessionTask? {
    guard let request = createRequest() else { return nil }
    
    return dataManager.dataTask(with: request) { [dispatchQueue, filename, completionHandler] data, response, error in
      dispatchQueue.async {
        if let error = error {
          completionHandler(.failure(BetRequestError.dataTaskErrorOccured(error.localizedDescription)))
        }
        
        guard let data = data,
              let image = UIImage(data: data) else {
          completionHandler(.failure(BetRequestError.noDataAvailable))
          return
        }
        
        let cdImage = BAImage(id: filename, image: image)
        completionHandler(.success(cdImage))
      }
    }
  }
}
