public protocol Request {
  associatedtype Data

  func dataTask(completionHandler: @escaping (Result<Data, Error>) -> Void) -> SessionTask?
}
