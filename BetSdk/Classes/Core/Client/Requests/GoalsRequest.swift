public class GoalsRequest: BaseRequest<[BAGoal]> {
  
  // MARK: - Init
  
  public init(dataManager: DataManager, matchId: String) {
    super.init(dataManager: dataManager,
               method: .get,
               route: .goals(matchId),
               headers: nil,
               dispatchQueue: .main)
  }
}
