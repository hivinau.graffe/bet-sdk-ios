public enum BetRequestError: Error {
  case dataTaskErrorOccured(String)
  case noDataAvailable
}

extension BetRequestError: Equatable {
  
  public static func ==(lhs: BetRequestError, rhs: BetRequestError) -> Bool {
    switch (lhs, rhs) {
    case (noDataAvailable, .noDataAvailable): return true
    case (dataTaskErrorOccured(let reason1), dataTaskErrorOccured(let reason2)):
      return reason1 == reason2
    default:
      return false
    }
  }
}
