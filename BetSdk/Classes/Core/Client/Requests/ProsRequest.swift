public class ProsRequest: BaseRequest<[BAPro]> {
  
  // MARK: - Init
  
  public init(dataManager: DataManager, matchId: String) {
    super.init(dataManager: dataManager,
               method: .get,
               route: .pros(matchId),
               headers: nil,
               dispatchQueue: .main)
  }
}
