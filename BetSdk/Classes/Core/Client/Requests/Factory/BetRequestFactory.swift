public final class BetRequestFactory: RequestFactory {
  
  // MARK: - Private properties
  
  private let dataManager: DataManager
  
  // MARK: - Init
  
  init(dataManager: DataManager) {
    self.dataManager = dataManager
  }
  
  // MARK: - RequestFactory methods
  
  public func betsRequest() -> BetsRequest {
    return BetsRequest(dataManager: dataManager)
  }
  
  public func betMatchesRequest(_ betId: String) -> MatchesRequest {
    return MatchesRequest(dataManager: dataManager, betId: betId)
  }
  
  public func matchProsRequest(_ matchId: String) -> ProsRequest {
    return ProsRequest(dataManager: dataManager, matchId: matchId)
  }
  
  public func imageRequest(_ filename: String) -> ImageRequest {
    return ImageRequest(dataManager: dataManager, filename: filename)
  }
  
  public func matchGoalsRequest(_ matchId: String) -> GoalsRequest {
    return GoalsRequest(dataManager: dataManager, matchId: matchId)
  }
}
