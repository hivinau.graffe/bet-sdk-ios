public protocol RequestFactory {
  func betsRequest() -> BetsRequest
  func betMatchesRequest(_ betId: String) -> MatchesRequest
  func matchProsRequest(_ matchId: String) -> ProsRequest
  func imageRequest(_ filename: String) -> ImageRequest
  func matchGoalsRequest(_ matchId: String) -> GoalsRequest
}
