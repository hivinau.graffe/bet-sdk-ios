public class BetsRequest: BaseRequest<[BABet]> {
  
  // MARK: - Init
  
  public init(dataManager: DataManager) {
    super.init(dataManager: dataManager,
               method: .get,
               route: .bets,
               headers: nil,
               dispatchQueue: .main)
  }
}
