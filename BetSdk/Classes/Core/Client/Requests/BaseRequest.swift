import Foundation

public class BaseRequest<Data: Decodable>: Request {
  
  // MARK: - Private properties
  
  internal let dataManager: DataManager
  internal let method: Method
  internal let route: Route
  internal let headers: Headers?
  internal let dispatchQueue: DispatchQueue
  
  // MARK: - Init
  
  public init(dataManager: DataManager,
              method: Method,
              route: Route,
              headers: Headers?,
              dispatchQueue: DispatchQueue) {
    self.dataManager = dataManager
    self.method = method
    self.route = route
    self.headers = headers
    self.dispatchQueue = dispatchQueue
  }
  
  // MARK: - Request methods
  
  public func dataTask(completionHandler: @escaping (Result<Data, Error>) -> Void) -> SessionTask? {
    guard let request = createRequest() else { return nil }
    
    return dataManager.dataTask(with: request) { [dispatchQueue, dataManager, completionHandler] data, response, error in
      dispatchQueue.async {
        if let error = error {
          completionHandler(.failure(BetRequestError.dataTaskErrorOccured(error.localizedDescription)))
        }
        
        guard let data = data else {
          completionHandler(.failure(BetRequestError.noDataAvailable))
          return
        }
        
        guard let objects = dataManager.decode(Data.self, from: data) else {
          completionHandler(.failure(BetRequestError.noDataAvailable))
          return
        }
        
        completionHandler(.success(objects))
      }
    }
  }
  
  // MARK: - Private methods
  
  internal func createRequest() -> URLRequest? {
    guard let url = dataManager.url(for: route) else { return nil }
    
    var request = URLRequest(url: url)
    request.httpMethod = "\(method)"
    headers?.keys
      .forEach { key in
        if let value = headers?.value(for: key) {
          request.addValue(value, forHTTPHeaderField: key)
        }
      }
    
    return request
  }
}
