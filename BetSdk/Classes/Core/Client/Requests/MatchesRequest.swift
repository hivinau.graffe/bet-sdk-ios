public class MatchesRequest: BaseRequest<[BAMatch]> {
  
  // MARK: - Init
  
  public init(dataManager: DataManager, betId: String) {
    super.init(dataManager: dataManager,
               method: .get, 
               route: .matches(betId),
               headers: nil,
               dispatchQueue: .main)
  }
}
