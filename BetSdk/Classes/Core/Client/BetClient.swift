@objc open class BetClient: NSObject, Client {
  
  // MARK: - Public properties
  
  open var delegate: BetClientDelegate?
  
  // MARK: - Private properties
  
  private let requestFactory: RequestFactory
  private var tasks: [String: SessionTask]
  
  // MARK: - Init
  
  public init(dataManager: DataManager) {
    self.requestFactory = dataManager.requestFactory()
    tasks = [:]
  }
  
  // MARK: - Client methods
  
  open func bets() {
    let request = requestFactory.betsRequest()
    let requestId = "\(#function)"
    
    tasks[requestId]?.cancel()
    tasks[requestId] = taskRequest(request) { [weak self] result in
      guard let self = self else { return }
      
      switch result {
      case .success(let bets): self.delegate?.clientDidReceive?(self, bets: bets)
      case .failure(let error): self.delegate?.clientDidReceive?(self, error: error)
      }
    }
    tasks[requestId]?.resume()
  }
  
  open func betMatches(_ betId: String) {
    let request = requestFactory.betMatchesRequest(betId)
    let requestId = "\(#function):\(betId)"
    
    tasks[requestId]?.cancel()
    tasks[requestId] = taskRequest(request) { [weak self, betId] result in
      guard let self = self else { return }
      
      switch result {
      case .success(let matches):
        let matchesWithBetId = matches.map { match -> BAMatch in
          match.betId = betId
          return match
        }
        self.delegate?.clientDidReceive?(self, matches: matchesWithBetId)
      case .failure(let error): self.delegate?.clientDidReceive?(self, error: error)
      }
    }
    tasks[requestId]?.resume()
  }
  
  open func matchPros(_ matchId: String) {
    let request = requestFactory.matchProsRequest(matchId)
    let requestId = "\(#function):\(matchId)"
    
    tasks[requestId]?.cancel()
    tasks[requestId] = taskRequest(request) { [weak self, matchId] result in
      guard let self = self else { return }
      
      switch result {
      case .success(let pros):
        let prosWithMatchId = pros.map { pro -> BAPro in
          pro.matchId = matchId
          return pro
        }
        self.delegate?.clientDidReceive?(self, pros: prosWithMatchId)
      case .failure(let error): self.delegate?.clientDidReceive?(self, error: error)
      }
    }
    tasks[requestId]?.resume()
  }
  
  open func image(_ filename: String) {
    let request = requestFactory.imageRequest(filename)
    let requestId = "\(#function):\(filename)"
    
    tasks[requestId]?.cancel()
    tasks[requestId] = taskRequest(request) { [weak self] result in
      guard let self = self else { return }
      
      switch result {
      case .success(let image): self.delegate?.clientDidReceive?(self, image: image)
      case .failure(let error): self.delegate?.clientDidReceive?(self, error: error)
      }
    }
    tasks[requestId]?.resume()
  }
  
  open func matchGoals(_ matchId: String) {
    let request = requestFactory.matchGoalsRequest(matchId)
    let requestId = "\(#function):\(matchId)"
    
    tasks[requestId]?.cancel()
    tasks[requestId] = taskRequest(request) { [weak self] result in
      guard let self = self else { return }
      
      switch result {
      case .success(let goals):
        let goalsWithMatchId = goals.map { goal -> BAGoal in
          goal.matchId = matchId
          return goal
        }
        self.delegate?.clientDidReceive?(self, goals: goalsWithMatchId)
      case .failure(let error): self.delegate?.clientDidReceive?(self, error: error)
      }
    }
    tasks[requestId]?.resume()
  }
  
  // MARK: - Private methods
  
  private func taskRequest<Data: Decodable>(_ request: BaseRequest<Data>,
                                            completionHandler: @escaping (Result<Data, Error>) -> Void) -> SessionTask? {
    return request.dataTask(completionHandler: completionHandler)
  }
}
