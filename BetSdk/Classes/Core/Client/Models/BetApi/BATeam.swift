@objc public final class BATeam: NSObject, Team {
  
  public typealias Colors = BAColors
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case id
    case name
    case image
    case colors
  }
  
  // MARK: - Bet properties
  
  public let id: String?
  public let name: String?
  public let image: String?
  public let colors: Colors?
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "id": "\(id ?? "N/A")",
      "colors": \(colors ?? BAColors(background: "N/A", primary: "N/A", secondary: "N/A", detail: "N/A")),
      "name": "\(name ?? "N/A")",
      "image": "\(image ?? "N/A")"
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(id: String?,
              name: String?,
              image: String?,
              colors: Colors?) {
    self.id = id
    self.name = name
    self.image = image
    self.colors = colors
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let id = try container.decode(String?.self, forKey: .id)
    let name = try container.decode(String?.self, forKey: .name)
    let image = try container.decode(String?.self, forKey: .image)
    let colors = try container.decode(Colors?.self, forKey: .colors)
    
    self.init(id: id, name: name, image: image, colors: colors)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(id, forKey: .id)
    try container.encode(name, forKey: .name)
    try container.encode(image, forKey: .image)
    try container.encode(colors, forKey: .colors)
  }
}

