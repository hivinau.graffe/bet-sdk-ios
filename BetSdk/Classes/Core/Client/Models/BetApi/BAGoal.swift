@objc public final class BAGoal: NSObject, Goal {
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case id
    case matchId = "match_id"
    case teamId = "team_id"
    case scorer
    case time
  }
  
  // MARK: - Bet properties
  
  public let id: String?
  public let teamId: String?
  public var matchId: String?
  public let scorer: String?
  public let time: Int
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "time": \(time),
      "id": "\(id ?? "N/A")",
      "match_id": "\(matchId ?? "N/A")",
      "team_id": "\(teamId ?? "N/A")",
      "scorer": "\(scorer ?? "N/A")"
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(id: String?,
              matchId: String?,
              teamId: String?,
              scorer: String?,
              time: Int) {
    self.id = id
    self.matchId = matchId
    self.teamId = teamId
    self.scorer = scorer
    self.time = time
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let id = try container.decode(String?.self, forKey: .id)
    let matchId = try? container.decode(String?.self, forKey: .matchId)
    let teamId = try container.decode(String?.self, forKey: .teamId)
    let scorer = try container.decode(String?.self, forKey: .scorer)
    let time = try container.decode(Int.self, forKey: .time)
    
    self.init(id: id, matchId: matchId, teamId: teamId, scorer: scorer, time: time)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(id, forKey: .id)
    try container.encode(matchId, forKey: .matchId)
    try container.encode(teamId, forKey: .teamId)
    try container.encode(scorer, forKey: .scorer)
    try container.encode(time, forKey: .time)
  }
}

