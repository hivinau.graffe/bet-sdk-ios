@objc public final class BAColors: NSObject, Colors {
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case background
    case primary
    case secondary
    case detail
  }
  
  // MARK: - Bet properties
  
  public let background: String?
  public let primary: String?
  public let secondary: String?
  public let detail: String?
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "secondary": "\(secondary ?? "N/A")",
      "primary": "\(primary ?? "N/A")",
      "detail": "\(detail ?? "N/A")",
      "background": "\(background ?? "N/A")"
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(background: String?,
              primary: String?,
              secondary: String?,
              detail: String?) {
    self.background = background
    self.primary = primary
    self.secondary = secondary
    self.detail = detail
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let background = try container.decode(String?.self, forKey: .background)
    let primary = try container.decode(String?.self, forKey: .primary)
    let secondary = try container.decode(String?.self, forKey: .secondary)
    let detail = try container.decode(String?.self, forKey: .detail)
    
    self.init(background: background,
              primary: primary,
              secondary: secondary,
              detail: detail)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(background, forKey: .background)
    try container.encode(primary, forKey: .primary)
    try container.encode(secondary, forKey: .secondary)
    try container.encode(detail, forKey: .detail)
  }
}

