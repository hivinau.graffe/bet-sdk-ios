@objc public final class BABet: NSObject, Bet {
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case id
    case locked = "is_locked"
    case priceToUnlock = "price_to_unlock"
    case matchesCount = "matches_count"
  }
  
  // MARK: - Bet properties
  
  public let id: String?
  public let locked: Bool
  public let priceToUnlock: Float
  public let matchesCount: Int
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "id": "\(id ?? "N/A")",
      "price_to_unlock": \(priceToUnlock),
      "is_locked": \(locked),
      "matches_count": \(matchesCount)
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(id: String?,
              locked: Bool,
              priceToUnlock: Float,
              matchesCount: Int) {
    self.id = id
    self.locked = locked
    self.priceToUnlock = priceToUnlock
    self.matchesCount = matchesCount
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let id = try container.decode(String?.self, forKey: .id)
    let locked = try container.decode(Bool.self, forKey: .locked)
    let priceToUnlock = try container.decode(Float.self, forKey: .priceToUnlock)
    let matchesCount = try container.decode(Int.self, forKey: .matchesCount)
    
    self.init(id: id, locked: locked, priceToUnlock: priceToUnlock, matchesCount: matchesCount)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(id, forKey: .id)
    try container.encode(locked, forKey: .locked)
    try container.encode(priceToUnlock, forKey: .priceToUnlock)
    try container.encode(matchesCount, forKey: .matchesCount)
  }
}

