import UIKit

@objc public final class BAImage: NSObject, Image {
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case id
    case image = "image"
  }
  
  // MARK: - Image properties
  
  public let id: String?
  public let image: UIImage?
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "id": "\(id ?? "N/A")",
      "image": "\(String(decoding: image?.pngData() ?? .init(), as: UTF8.self))"
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(id: String?,
              image: UIImage?) {
    self.id = id
    self.image = image
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let id = try container.decode(String?.self, forKey: .id)
    let data = try container.decode(Data.self, forKey: .image)
    let image = UIImage(data: data)
    
    self.init(id: id, image: image)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(id, forKey: .id)
    guard let image = image,
          let data = image.jpegData(compressionQuality: 1.0) else { return }
      
    try container.encode(data, forKey: .image)
  }
}
