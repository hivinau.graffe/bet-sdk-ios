@objc public final class BAPro: NSObject, Pro {
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case id
    case matchId = "match_id"
    case label
    case won = "is_won"
    case type
  }
  
  // MARK: - Bet properties
  
  public let id: String?
  public let label: String?
  public let won: Bool
  public var matchId: String?
  public var type: String?
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "id": "\(id ?? "N/A")",
      "match_id": "\(matchId ?? "N/A")",
      "label": "\(label ?? "N/A")",
      "type": "\(type ?? "N/A")",
      "is_won": \(won)
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(id: String?,
              matchId: String?,
              label: String?,
              won: Bool,
              type: String?) {
    self.id = id
    self.matchId = matchId
    self.label = label
    self.won = won
    self.type = type
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let id = try container.decode(String?.self, forKey: .id)
    let matchId = try? container.decode(String?.self, forKey: .matchId)
    let label = try container.decode(String?.self, forKey: .label)
    let won = try container.decode(Bool.self, forKey: .won)
    let type = try container.decode(String?.self, forKey: .type)
    
    self.init(id: id, matchId: matchId, label: label, won: won, type: type)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(id, forKey: .id)
    try container.encode(matchId, forKey: .matchId)
    try container.encode(label, forKey: .label)
    try container.encode(won, forKey: .won)
    try container.encode(type, forKey: .type)
  }
}
