@objc public final class BAMatch: NSObject, Match {
  
  public typealias Team = BATeam
  
  // MARK: - CodingKeys
  
  private enum CodingKeys: String, CodingKey {
    case id
    case betId = "bet_id"
    case started = "is_started"
    case live = "is_live"
    case date
    case time
    case teams
    case prosCount = "pros_count"
  }
  
  // MARK: - TeamsKeys
  
  private enum TeamsKeys: String, CodingKey {
    case teams
  }
  
  // MARK: - Match properties
  
  public let id: String?
  public var betId: String?
  public let started: Bool
  public let live: Bool
  public let date: String?
  public let time: String?
  public let teams: [Team]
  public let prosCount: Int
  
  // MARK: - CustomStringConvertible properties
  
  public override var description: String {
    """
    {
      "bet_id": "\(betId ?? "N/A")",
      "is_started": \(started),
      "time": "\(time ?? "N/A")",
      "id": "\(id ?? "N/A")",
      "teams": \(teams),
      "is_live": \(live),
      "date": "\(date ?? "N/A")",
      "pros_count": \(prosCount)
    }
    """.replacingOccurrences(of: "[\\n\\s]", with: "", options: .regularExpression)
  }
  
  // MARK: - Init
  
  public init(id: String?,
              betId: String?,
              started: Bool,
              live: Bool,
              date: String?,
              time: String?,
              teams: [Team],
              prosCount: Int) {
    self.id = id
    self.betId = betId
    self.started = started
    self.live = live
    self.date = date
    self.time = time
    self.teams = teams
    self.prosCount = prosCount
  }
  
  // MARK: - Decodable methods
  
  public convenience init(from decoder: Swift.Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    let id = try container.decode(String?.self, forKey: .id)
    let betId = try? container.decode(String?.self, forKey: .betId)
    let started = try container.decode(Bool.self, forKey: .started)
    let live = try container.decode(Bool.self, forKey: .live)
    let date = try container.decode(String?.self, forKey: .date)
    let time = try container.decode(String?.self, forKey: .time)
    let teams = try container.decode([Team].self, forKey: .teams)
    let prosCount = try container.decode(Int.self, forKey: .prosCount)
    
    self.init(id: id,
              betId: betId,
              started: started,
              live: live,
              date: date,
              time: time,
              teams: teams,
              prosCount: prosCount)
  }
  
  // MARK: - Encodable methods
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(id, forKey: .id)
    try container.encode(betId, forKey: .betId)
    try container.encode(started, forKey: .started)
    try container.encode(live, forKey: .live)
    try container.encode(date, forKey: .date)
    try container.encode(time, forKey: .time)
    try container.encode(teams, forKey: .teams)
    try container.encode(prosCount, forKey: .prosCount)
  }
}
