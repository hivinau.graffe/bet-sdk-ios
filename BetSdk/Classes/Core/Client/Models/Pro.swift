public protocol Pro: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  var id: String? { get }
  var matchId: String? { get }
  var label: String? { get }
  var won: Bool { get }
  var type: String? { get }
}
