public protocol Match: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  associatedtype Team
  
  var id: String? { get }
  var betId: String? { get }
  var started: Bool { get }
  var live: Bool { get }
  var date: String? { get }
  var time: String? { get }
  var teams: [Team] { get }
  var prosCount: Int { get }
}
