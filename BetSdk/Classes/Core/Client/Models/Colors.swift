public protocol Colors: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  var background: String? { get }
  var primary: String? { get }
  var secondary: String? { get }
  var detail: String? { get }
}
