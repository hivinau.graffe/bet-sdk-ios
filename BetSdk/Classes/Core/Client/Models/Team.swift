public protocol Team: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  associatedtype Colors
  
  var id: String? { get }
  var name: String? { get }
  var image: String? { get }
  var colors: Colors? { get }
}
