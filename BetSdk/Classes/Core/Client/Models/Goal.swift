public protocol Goal: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  var id: String? { get }
  var matchId: String? { get }
  var teamId: String? { get }
  var scorer: String? { get }
  var time: Int { get }
}
