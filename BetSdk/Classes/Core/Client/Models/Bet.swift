public protocol Bet: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  var id: String? { get }
  var locked: Bool { get }
  var priceToUnlock: Float { get }
  var matchesCount: Int { get }
}
