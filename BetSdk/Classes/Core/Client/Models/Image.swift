import UIKit

public protocol Image: NSObjectProtocol, Identifiable, Codable, CustomStringConvertible {
  var id: String? { get }
  var image: UIImage? { get }
}
