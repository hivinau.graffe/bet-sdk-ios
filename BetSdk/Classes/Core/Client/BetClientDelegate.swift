@objc public protocol BetClientDelegate: NSObjectProtocol {
  @objc optional func clientDidReceive(_ client: BetClient, bets: [BABet])
  @objc optional func clientDidReceive(_ client: BetClient, matches: [BAMatch])
  @objc optional func clientDidReceive(_ client: BetClient, pros: [BAPro])
  @objc optional func clientDidReceive(_ client: BetClient, image: BAImage)
  @objc optional func clientDidReceive(_ client: BetClient, error: Error)
  @objc optional func clientDidReceive(_ client: BetClient, goals: [BAGoal])
}
